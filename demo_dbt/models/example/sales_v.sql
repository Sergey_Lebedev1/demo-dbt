{{
  config(
    materialized='view'
  )
}}

select s.qty * s.price as sum,
	o.order_num  ,
	c.customer_name,
	o.order_date
	from dbt.sales s
	inner join dbt."order" o
	on s.header_id = o.header_id
	inner join dbt.customers c
	on s.customer_id = c.customer_id