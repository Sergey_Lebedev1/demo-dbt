
{{
  config(
    materialized='view'
  )
}}

select res.id,
		res.cnt,
		res.product,
		res.sales_sum,
		res.sale_date
	from (select sel.id,
		sel.cnt,
		pr.product_desc as product,
		sel.sales_sum as sales_sum,
		sel.sale_date,
		row_number() over (partition by sel.id) as row_cnt
		    from (select s.product_id as id,
		        count(s.product_id) as cnt,
		        sum(s.qty * s.price) as sales_sum,
		        s.product_id,
		        s.sale_date
		        from {{ ref('s_sales') }} s
		        group by s.product_id , s.sale_date
	    ) sel
	left join dbt.products pr
	on pr.product_id = sel.product_id
	) res
	where --res.row_cnt = 1 and
	    res.sale_date >= '{{ var("start_date") }}'
order by sale_date desc

