
select sum(s.qty * s.price) as sales_sum,
	c.customer_name,
	s.sale_date
	from {{ ref('s_sales') }} s
	inner join dbt."order" o
	on s.header_id = o.header_id
	inner join dbt.customers c
	on s.customer_id = c.customer_id
	group by c.customer_name, s.sale_date
    order by sales_sum desc